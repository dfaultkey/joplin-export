# joplin-export

This tool can be used to export joplin notes to a directory structure.

Original author of the code is https://gitlab.com/stavros

I pushed it here, because I was unable to fork his repo and also there might some functional differences between his script and the modified script. Old script is intended for creating static webpages. This modified script will straight out spit the joplin notes.
